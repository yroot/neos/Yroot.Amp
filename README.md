# YROOT Amp Fusion

## Components
The AMP HTML components.
[Components / Tags](https://www.ampproject.org/docs/reference/components)

### Impemented
- amp-img

### Comimg soon
- amp-ad
- amp-ad-exit
- amp-analytics
- amp-auto-ads
- amp-call-tracking
- amp-experiment
- amp-pixel
- amp-sticky-ad
- amp-access-laterpay
- amp-access
- amp-bind
- amp-form
- amp-gist
- amp-install-serviceworker
- amp-list
- amp-live-list
- amp-mustache
- amp-selector
- amp-user-notification
- amp-web-push
- amp-accordion
- amp-app-banner
- amp-carousel
- amp-fx-flying-carpet
- amp-fx-parallax
- amp-iframe
- amp-lightbox
- amp-position-observer
- amp-sidebar
- amp-3q-player
- amp-anim
- amp-apester-media
- amp-audio
- amp-brid-player
- amp-brightcove
- amp-dailymotion
- amp-google-vrview-image
- amp-hulu
- amp-ima-video
- amp-image-lightbox
- amp-imgur
- amp-izlesene
- amp-jwplayer
- amp-kaltura-player
- amp-nexxtv-player
- amp-o2-player
- amp-ooyala-player
- amp-playbuzz
- amp-reach-player
- amp-soundcloud
- amp-springboard-player
- amp-video
- amp-vimeo
- amp-youtube
- amp-animation
- amp-dynamic-css-classes
- amp-fit-text
- amp-font
- amp-timeago
- amp-viz-vega
- amp-facebook-comments
- amp-facebook-like
- amp-facebook
- amp-gfycat
- amp-instagram
- amp-pinterest
- amp-reddit
- amp-social-share
- amp-twitter
- amp-vine
- amp-vk

## Validation
You can validate a page by adding `#development=1` to the URL - the result is visible in console.

**IMPORTANT:**
The validation will fail if you are logged in (frontend AND backend) because of the backend scripts!